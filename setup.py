import setuptools

# Read the README.md file to add it to the long_description attribute below
with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pyspectraltilt",
    version="0.0.1",
    author="Stefano Tronci",
    author_email="stefano.tronci@protonmail.com",
    description="A Python package for JOS's at al spectral tilt filter design.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/stefano-tronci/pyspectraltilt",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=[
       'numpy',
        'scipy',
    ]
)
