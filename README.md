# pySpectralTilt

Spectral Tilt filter design, in Python, following [Julius Orion Smith and Harrison Freeman Smith](https://arxiv.org/pdf/1606.06154.pdf) and its [Faust](https://github.com/grame-cncm/faustlibraries/blob/9082369d5f30b9c8e8c04279fbd5087) implementation.
