import numpy as np


def bilinear_coefficient(
        angular_frequency: float,
        sample_rate: float
) -> float:
    return angular_frequency / np.tan(0.5 * angular_frequency / sample_rate)


def bilinear_prewarp(
        coefficient: float,
        angular_frequency: float,
        sample_rate: float
) -> float:
    return coefficient * np.tan(0.5 * angular_frequency / sample_rate)


def compute_bilinear_filter(
        lower_angular_frequency: float,
        pole_ratio: float,
        order: int,
        fractional_order: float,
        prewarp_coefficient: float,
        transform_coefficient: float,
        sample_rate: float
):
    # Analog frequencies for current zero and pole:
    neg_zero = lower_angular_frequency * pole_ratio ** (order - fractional_order)
    neg_pole = lower_angular_frequency * pole_ratio ** order

    # Map (warp) to digital frequency:
    neg_zero_pw = bilinear_prewarp(prewarp_coefficient, neg_zero, sample_rate)
    neg_pole_pw = bilinear_prewarp(prewarp_coefficient, neg_pole, sample_rate)

    # Assembling coefficients for zero-pole term:

    # Analog pre-warped (--> Order):
    b_a = np.array([neg_zero_pw, 1.0])
    a_a = np.array([neg_pole_pw, 1.0])
    g_a = a_a[0] / b_a[0]
    b_a *= g_a

    # Final digital form:

    g_d = 1 / (a_a[0] + transform_coefficient)

    b_d_0 = (b_a[0] + b_a[1] * transform_coefficient) * g_d
    b_d_1 = (b_a[0] - b_a[1] * transform_coefficient) * g_d

    a_d_0 = 1.0
    a_d_1 = (a_a[0] - transform_coefficient) * g_d

    return b_d_0, b_d_1, a_d_0, a_d_1


def spectral_tilt(
        integer_order: int,
        fractional_order: float,
        lower_frequency: float,
        upper_frequency: float,
        sample_rate: float
) -> np.ndarray:
    # assert integer_order >= 0.0
    # assert -1.0 <= fractional_order <= 1.0
    # assert lower_frequency > 0.0
    # assert lower_frequency < 0.5 * sample_rate
    # assert upper_frequency < 0.5 * sample_rate
    # assert upper_frequency > lower_frequency

    if integer_order % 2 != 0:
        integer_order += 1

    lower_angular_frequency = 2 * np.pi * lower_frequency
    pole_ratio = (upper_frequency / lower_frequency)**(1 / (integer_order - 1))

    c_prewarp = bilinear_coefficient(lower_angular_frequency, sample_rate)
    c_finalise = bilinear_coefficient(1.0, sample_rate)

    query_orders = [n for n in range(integer_order) if n % 2 == 0]

    sos = np.zeros((len(query_orders), 6))
    ass = 0

    for o in query_orders:

        b_d_0, b_d_1, a_d_0, a_d_1 = compute_bilinear_filter(
            lower_angular_frequency=lower_angular_frequency,
            pole_ratio=pole_ratio,
            order=o,
            fractional_order=fractional_order,
            prewarp_coefficient=c_prewarp,
            transform_coefficient=c_finalise,
            sample_rate=sample_rate
        )

        b_d_0_n, b_d_1_n, a_d_0_n, a_d_1_n = compute_bilinear_filter(
            lower_angular_frequency=lower_angular_frequency,
            pole_ratio=pole_ratio,
            order=o+1,
            fractional_order=fractional_order,
            prewarp_coefficient=c_prewarp,
            transform_coefficient=c_finalise,
            sample_rate=sample_rate
        )

        sos[ass, 0] = b_d_0 * b_d_0_n
        sos[ass, 1] = b_d_0 * b_d_1_n + b_d_1 * b_d_0_n
        sos[ass, 2] = b_d_1 * b_d_1_n

        sos[ass, 3] = 1.0
        sos[ass, 4] = a_d_1 + a_d_1_n
        sos[ass, 5] = a_d_1 * a_d_1_n

        ass += 1

        # g_u = np.abs(sos[o, 3] + sos[o, 4]) / np.abs(sos[o, 0] + sos[o, 1])
        #
        # sos[o, :2] *= g_u

    return sos
