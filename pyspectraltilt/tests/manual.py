from pyspectraltilt import lib
import numpy as np
from scipy import signal
import matplotlib.pyplot as plt

fs = 48e3
lower = 10
upper = 0.9 * 0.5 * fs
order = 16

alpha_vals = np.arange(-2, 2 + 0.1, 0.1)
lower_vals = np.array([0.1, 1, 5, 10, 15, 20])

plt.rc('font', size=20)
fig = plt.figure()
ax_m, ax_p = fig.subplots(2, 1, sharex=True)
leg_items = []

for alpha in alpha_vals:

    g = lib.spectral_tilt(
        integer_order=order,
        fractional_order=alpha,
        lower_frequency=lower,
        upper_frequency=upper,
        sample_rate=fs
    )

# for lower in lower_vals:
#
#     g = lib.spectral_tilt(
#         integer_order=order,
#         fractional_order=-0.5,
#         lower_frequency=lower,
#         upper_frequency=upper,
#         sample_rate=fs
#     )

    f, H = signal.sosfreqz(g, worN=2**20, fs=fs)

    ax_m.semilogx(f, 20 * np.log10(np.abs(H)))
    # ax_m.semilogx(f, 20 * np.log10((2 * np.pi * f)**alpha / (2 * np.pi * lower)**alpha))
    ax_m.set_xlabel('Frequency [Hz]')
    ax_m.set_ylabel('Magnitude Response [dB]')
    ax_m.grid(True, which='both')
    ax_p.semilogx(f, np.angle(H))
    ax_p.set_xlabel('Frequency [Hz]')
    ax_p.set_ylabel('Phase Response [rad]')
    ax_p.grid(True, which='both')

    leg_items.append(r'$\alpha$ = ' + str(np.round(alpha, 1)))
    # leg_items.append(r'$f_{l}$ = ' + str(lower))

# fig.legend(['Design', 'Target'])
fig.legend(leg_items)
ax_m.set_xlim(1, 0.5 * fs)
plt.show()
